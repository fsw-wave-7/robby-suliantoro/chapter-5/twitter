const {successResponse} = require('../helpers/response.js')

class Post {
    constructor () {
        this.post = [{
            'text': 'Postingan pertama saya',
            'created_at': '2021-03-11 21:10:00',
            'username': 'jono'
        },
        {
            'text': 'Postingan kedua saya',
            'created_at': '2021-03-11 21:15:00',
            'username': 'jono'
        }
    
        ]
    }

    getPost = (req, res) => {
    /*    //res.json(this.post)
        res.status(200).json({
            data: this.post,
            meta: {
                code: 200,
                message: 'Sukses mengambil data',
                total: this.post.length
            }
        }) */
        successResponse(
            res,
            200,
            this.post,
            { total: this.post.length }
        )
        }
    
    getDetailPost = (req, res) => {
    /*    //const index = req.params.index
        //res.json(this.post[index])
        const index = req.params.index
        res.status(200).json({
            data: this.post[index],
            meta: {
                code: 200,
                message: 'Sukses mengambil data'
            }
        }) */
        const index = req.params.index
        successResponse(res,200,this.post[index])
    }

    insertPost = (req, res) => {
        const body = req.body
        //console.log(body)
        const param = {
            'text': body.text,
            'created_at': new Date(),
            'username': body.username
        }

    /*    //helper
        this.post.push(param)
        res.status(201).json({
            data: param,
            meta: {
                code: 201,
                message: 'Sukses menyimpan data'
            }
        }) */

        this.post.push(param)
        successResponse(res, 201, param)
    }

    updatePost = (req, res) => {
        const index = req.params.index
        const body = req.body

        this.post[index].text = body.text
        this.post[index].username = body.username
        this.post[index].created_at = body.created_at

    /*    res.status(200).json({
            data:this.post[index],
            meta: {
                code: 200,
                message: 'Sukses mengubah data'
            }
        }) */
        successResponse(res, 200, this.post[index])
    }

    deletePost = (req, res) => {
        const index = req.params.index
        this.post.splice(index, 1);

    /*    res.status(200).json({
            data: null,
            meta: {
                code: 200,
                message: 'Sukses menghapus data'
            }
        }) */
        successResponse(res, 200, null)
    }
}

module.exports = Post


//create, READ, UPDATE, delete
//CRUD