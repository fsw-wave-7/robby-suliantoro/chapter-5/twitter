const {successResponse} = require('../helpers/response.js')

class Post2 {
    constructor () {
        this.post2 = [{
            'nama_lengkap': 'Dhian Alyusi',
            'user_name': 'dhian',
            'password': 'alyusi1234',
            'email': 'dhian.alyusi@gmail.com'
        },
        {
            'nama_lengkap': 'Robby Suliantoro',
            'user_name': 'robby',
            'password': 'robby5678',
            'email': 'robby.suliantoro@gmail.com'
        }
    
        ]
    }

getPost2 = (req, res) => {
    successResponse(res, 200, this.post2, { total: this.post2.length })
        }
    
getDetailPost2 = (req, res) => {
    const index2 = req.params.index2
        successResponse(res,200,this.post2[index2])
    }

insertPost2 = (req, res) => {
    const body = req.body
    const param = {
        'nama_lengkap': body.nama_lengkap,
        'user_name': body.user_name,
        'password': body.password,
        'email': body.email
        }

    this.post2.push(param)
        successResponse(res, 201, param)
    }

updatePost2 = (req, res) => {
        const index2 = req.params.index2
        const body = req.body

        this.post2[index2].nama_lengkap = body.nama_lengkap
        this.post2[index2].user_name = body.user_name
        this.post2[index2].password = body.password
        this.post2[index2].email = body.email

        successResponse(res, 200, this.post2[index2])
    }

deletePost2 = (req, res) => {
        const index2 = req.params.index2
        this.post2.splice(index2, 1);

        successResponse(res, 200, null)
    }
}

module.exports = Post2
