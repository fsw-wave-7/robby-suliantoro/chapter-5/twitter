const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')

const app = express()
const port = 3000

const jsonParser = bodyParser.json()

const Post = require('./controllers/post.js')

const post = new Post

app.get('/', function(req, res) {
    //res.json({
    //    'hello': 'world'
    //})
    res.send("HALO ")
    //res.sendFile(path.join(__dirname, './index.html'))
})

app.get('/post', post.getPost)
app.get('/post/:index', post.getDetailPost)
app.post('/post', jsonParser, post.insertPost)
app.put('/post/:index', jsonParser, post.updatePost)
app.delete('/post/:index', post.deletePost)

// user
// user.js


app.listen(port, () => { console.log("Server berhasil dijalankan!!") })

//

// / --> local host:3000 -> {'hello' : 'world'}
// bikin end point di timeline twitter
// / post --> localhost:3000/post

// POST
// - text
// - created_at
// - username