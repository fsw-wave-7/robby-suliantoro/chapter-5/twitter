const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')

const app = express()
const port = 3001

const jsonParser = bodyParser.json()

const Post2 = require('./controllers/post2.js')

const post2 = new Post2

app.get('/', function(req, res) {
    res.send("HALO ")
})

app.get('/post2', post2.getPost2)
app.get('/post2/:index2', post2.getDetailPost2)
app.post('/post2', jsonParser, post2.insertPost2)
app.put('/post2/:index2', jsonParser, post2.updatePost2)
app.delete('/post2/:index2', post2.deletePost2)

app.listen(port, () => { console.log("Server berhasil dijalankan!!") })
